package org.ctcahealth;


import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;

import java.util.Arrays;


public class DataSrcTable {

    public static void main(String[] args) throws Exception {
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
                //.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:8000", "us-west-2"))
                .build();
        DynamoDB dynamoDB = new DynamoDB(client);

        String tableName = "DataSrc";

        try {
            System.out.println("Attempting to create table; please wait...");
            Table table = dynamoDB.createTable(tableName,
                    Arrays.asList(new KeySchemaElement("DATA_SRC_ID", KeyType.HASH)), // Partition
                    Arrays.asList(
                            new AttributeDefinition("DATA_SRC_ID", ScalarAttributeType.N),
                            new AttributeDefinition("DATA_SRC_NM", ScalarAttributeType.S),
                            new AttributeDefinition("CLIENT_ID", ScalarAttributeType.N),
                            new AttributeDefinition("DATA_SRC_TYPE_ID", ScalarAttributeType.N),
                            new AttributeDefinition("DATA_SRC_STATUS_CD", ScalarAttributeType.S)),
                            new ProvisionedThroughput(10L, 10L));
            table.waitForActive();
            System.out.println("Success.  Table status: " + table.getDescription().getTableStatus());

        }
        catch (Exception e) {
            System.err.println("Unable to create table: ");
            System.err.println(e.getMessage());
        }

    }
}



