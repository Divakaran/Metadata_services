package org.ctcahealth;


import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;

import java.util.Arrays;


public class IngestFileDetail {

    public static void main(String[] args) throws Exception {
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
                //.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:8000", "us-west-2"))
                .build();
        DynamoDB dynamoDB = new DynamoDB(client);

        String tableName = "DataSrc";

        try {
            System.out.println("Attempting to create table; please wait...");
            Table table = dynamoDB.createTable(tableName,
                    Arrays.asList(new KeySchemaElement("FILE_DETAIL_ID", KeyType.HASH)), // Partition
                    Arrays.asList(
                            new AttributeDefinition("FILE_DETAIL_ID", ScalarAttributeType.N),
                            new AttributeDefinition("FILE_DETAIL_NM", ScalarAttributeType.S),
                            new AttributeDefinition("DATA_SRC_ID", ScalarAttributeType.N),
                            new AttributeDefinition("FILE_DETAIL_STATUS_CD", ScalarAttributeType.N),
                            new AttributeDefinition("FILE_DETAIL_SIZE", ScalarAttributeType.N),
                            new AttributeDefinition("FILE_DETAIL_NBR_ROWS", ScalarAttributeType.N),
                            new AttributeDefinition("FILE_DETAIL_RCV_DTTM", ScalarAttributeType.S),
                            new AttributeDefinition("FILE_DETAIL_PATH", ScalarAttributeType.S),
                            new AttributeDefinition("FILE_DETAIL_TYPE_CD", ScalarAttributeType.S),
                            new AttributeDefinition("DELIMITER", ScalarAttributeType.S),
                            new AttributeDefinition("TEXT_QUAL", ScalarAttributeType.S),
                            new AttributeDefinition("FIXED_WIDTH", ScalarAttributeType.S),
                            new AttributeDefinition("SOURCE_TABLE_NM", ScalarAttributeType.S)),
                            new ProvisionedThroughput(10L, 10L));
            table.waitForActive();
            System.out.println("Success.  Table status: " + table.getDescription().getTableStatus());

        }
        catch (Exception e) {
            System.err.println("Unable to create table: ");
            System.err.println(e.getMessage());
        }

    }
}



